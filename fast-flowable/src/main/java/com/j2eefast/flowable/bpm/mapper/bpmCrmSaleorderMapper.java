package com.j2eefast.flowable.bpm.mapper;

import com.j2eefast.flowable.bpm.entity.bpmCrmSaleorderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * bpm_crm_saleorderDAO接口
 * @author: yhli
 * @date 2020-06-15 10:19
 */
public interface bpmCrmSaleorderMapper extends BaseMapper<bpmCrmSaleorderEntity> {
    
}
