package com.j2eefast.flowable.bpm.controller;

import com.j2eefast.common.core.business.annotaion.BussinessLog;
import com.j2eefast.common.core.enums.BusinessType;
import com.j2eefast.common.core.utils.*;
import com.j2eefast.flowable.bpm.entity.BpmTaskFromEntity;
import com.j2eefast.flowable.bpm.service.BpmTaskFromService;
import com.j2eefast.framework.annotation.RepeatSubmit;
import com.j2eefast.common.core.controller.BaseController;
import com.j2eefast.framework.utils.UserUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.ui.ModelMap;
import java.util.Map;
import java.util.Date;
import org.springframework.web.bind.annotation.*;
import com.j2eefast.flowable.bpm.entity.bpmCrmSaleorderEntity;
import com.j2eefast.flowable.bpm.service.bpmCrmSaleorderService;

/**
 * 销售订单页面控制器
 * @author yhli
 * @date 2020-06-15 10:19
 */
@Controller
@RequestMapping("/bpm/saleorder")
public class bpmCrmSaleorderController extends BaseController{

    private String prefix = "modules/bpm/saleorder";
    @Autowired
    private bpmCrmSaleorderService bpmCrmSaleorderService;
    /**
    * 必须注入实例关联表单服务
    */
    @Autowired
    private BpmTaskFromService bpmTaskFromService;


    @RequiresPermissions("bpm:saleorder:view")
    @GetMapping()
    public String saleorder(){
        return prefix + "/saleorder";
    }
        
    @RequestMapping("/list")
    @RequiresPermissions("bpm:saleorder:list")
    @ResponseBody
    public ResponseData list(@RequestParam Map<String, Object> params) {
		PageUtil page = bpmCrmSaleorderService.findPage(params);
		return success(page);
    }    
            
    @GetMapping("/add")
    public String add(){
        return prefix + "/add";
    }
        /**
         * 定义关联表单申请表单URL对应此处
         */
        @GetMapping("/add/{id}")
        public String add(@PathVariable("id") Long id, ModelMap mmp){
            //通过页面传入的表单ID查询表单关联信息
            BpmTaskFromEntity bpmTaskFrom = bpmTaskFromService.getTaskFromById(id);
            mmp.put("bpmTaskFrom", bpmTaskFrom);
            mmp.put("user", UserUtils.getUserInfo());
            mmp.put("salecode",OrderCodeFactory.getOrderCode(UserUtils.getUserId()));
            return prefix + "/add";
        }

    /**
     * 新增
     */
    @RepeatSubmit
    @RequiresPermissions("bpm:saleorder:add")
    @BussinessLog(title = "销售订单", businessType = BusinessType.INSERT)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData addbpmCrmSaleorder(@Validated bpmCrmSaleorderEntity bpmCrmSaleorder){
        //校验参数
        ValidatorUtil.validateEntity(bpmCrmSaleorder);
        return bpmCrmSaleorderService.savebpmCrmSaleorder(bpmCrmSaleorder);
    }    
    
    /**
     * 修改
     */
    @RequiresPermissions("bpm:saleorder:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap){
        bpmCrmSaleorderEntity bpmCrmSaleorder = bpmCrmSaleorderService.getbpmCrmSaleorderById(id);
        mmap.put("bpmCrmSaleorder", bpmCrmSaleorder);
        return prefix + "/edit";
    }

    /**
     * 修改保存销售订单
     */
    @RepeatSubmit
    @RequiresPermissions("bpm:saleorder:edit")
    @BussinessLog(title = "销售订单", businessType = BusinessType.UPDATE)
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData editbpmCrmSaleorder(bpmCrmSaleorderEntity bpmCrmSaleorder){
		ValidatorUtil.validateEntity(bpmCrmSaleorder);
        return bpmCrmSaleorderService.updatebpmCrmSaleorderById(bpmCrmSaleorder)? success(): error("修改失败!");
    }    

    /**
     * 删除
     */
    @RepeatSubmit
    @BussinessLog(title = "销售订单", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @RequiresPermissions("bpm:saleorder:del")
    @ResponseBody
    public ResponseData del(String[] ids) {
      return bpmCrmSaleorderService.deletebpmCrmSaleorderByIds(ids)? success(): error("删除失败!");
    }    
}
